using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIGame : MonoBehaviour {
    [Header("Game UI")]
    [SerializeField]
    private TextMeshProUGUI
        points,
        timer;

    [SerializeField] private Image muteBtn;
    [SerializeField] private Sprite[] muteImages;
    [SerializeField]
    private GameObject
        panelPause,
        panelHelp;

    [Header("End Screen")]
    [SerializeField]
    private GameObject panelEndScreen;
    [SerializeField]
    private GameObject
        panelCollision,
        panelTimer;
    [SerializeField] private TextMeshProUGUI endPointsCollision, endPointsTimer;

    private string localizedPoints, localizedTimer;

    private async void Start() {
        localizedPoints = await bunGames.Localization.LocalizationManager.GetLocalizedUIString("UI_POINT_DISPLAY");
        localizedTimer = await bunGames.Localization.LocalizationManager.GetLocalizedUIString("UI_TIMER_DISPLAY");

        int imgId = !SoundManager.Instance.muted ? 0 : 1;
        muteBtn.sprite = muteImages[imgId];

        OnPointAdded(null);
        OnTimerDown(-1);
    }

    private void OnEnable() {
        Point.OnPointAdded += OnPointAdded;
        TimeManager.OnTimePassed += OnTimerDown;
        GameManager.OnGameOver += OnGameOver;
        GameManager.OnReceiveReward += OnReceiveReward;
    }

    private void OnDisable() {
        Point.OnPointAdded -= OnPointAdded;
        TimeManager.OnTimePassed -= OnTimerDown;
        GameManager.OnGameOver -= OnGameOver;
        GameManager.OnReceiveReward -= OnReceiveReward;
    }

    public void OnBackBtn() {
        Time.timeScale = 1f;
        GameManager.Instance.points = 0;
        SoundManager.PlayBGM(SoundManager.Sounds.MENU_BGM);
        SceneManager.LoadSceneAsync(0);
    }

    public void OnHelpBtn() {
        panelHelp.SetActive(!panelHelp.activeSelf);
    }

    public void OnPause() {
        panelPause.SetActive(GameManager.Instance.PauseGame());
        panelHelp.SetActive(false);
    }

    public void OnMute() {
        bool muted = SoundManager.Instance.OnMute();
        int imgId = !muted ? 0 : 1;
        muteBtn.sprite = muteImages[imgId];
    }

    private void OnPointAdded(Point point) {
        points.SetText(string.Format(localizedPoints, GameManager.Instance.points));
        endPointsCollision.SetText(GameManager.Instance.points.ToString());
        endPointsTimer.SetText(GameManager.Instance.points.ToString());
    }

    private void OnTimerDown(int timeLeft) {
        if (timeLeft == -1) {
            timer.SetText(string.Format(localizedTimer, GameManager.Instance.TimeLimit));
            return;
        }
        timer.SetText(string.Format(localizedTimer, timeLeft));

        if(timeLeft == 5) {
            SoundManager.PlaySFX(SoundManager.Sounds.CLOCK);
        }

        if (timeLeft <= 0) {
            //game over
            GameManager.Instance.TriggerGameOver(GameManager.GameOverReason.TIMER);
        }
    }

    private void OnGameOver(GameManager.GameOverReason reason) {
        if(GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.ARCADE) {
            if(PlayerPrefs.GetInt("ArcadeHighscore") < GameManager.Instance.points) {
                PlayerPrefs.SetInt("ArcadeHighscore", GameManager.Instance.points);
                PlayerPrefs.Save();
            }
        }
        else {
            if (PlayerPrefs.GetInt("TimedHighscore") < GameManager.Instance.points) {
                PlayerPrefs.SetInt("TimedHighscore", GameManager.Instance.points);
                PlayerPrefs.Save();
            }
        }

        panelCollision.SetActive(reason == GameManager.GameOverReason.COLLISION);
        panelTimer.SetActive(reason == GameManager.GameOverReason.TIMER);

        panelEndScreen.SetActive(true);
        SoundManager.PlaySFX(SoundManager.Sounds.FINAL);
    }


    private void OnReceiveReward(GameManager.AdRewards reward) {
        panelEndScreen.SetActive(false);
    }

    public void Test(int reward) {
        GameManager.Instance.TriggerReward((GameManager.AdRewards)reward);
    }
}
