using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

public class SnakeColorManager : MonoBehaviour {
    public enum Colors { BLUE, GREEN, ORANGE, PINK, PURPLE, RED, TEAL, WHITE, YELLOW }

    [SerializeField] private AssetReference tailReference;
    [SerializeField] private Point point;

    [SerializeField] private Vector2 boundary;

    [System.Serializable]
    public class SpriteColors {
        public Colors color;
        public AssetReference tailSprite;
        public AssetReference pointSprite;
    }

    [SerializeField]
    private SpriteColors[] tailColors;

    private Dictionary<Colors, SpriteColors> spriteColorsDict = new Dictionary<Colors, SpriteColors>();

    private void Start() {
        foreach (SpriteColors spriteColors in tailColors) {
            spriteColorsDict.Add(spriteColors.color, spriteColors);
        }

        tailColors = null;

        SpawnPoint();
    }

    private void SpawnPoint() {
        //reposition and change its color
        int randomColor = UnityEngine.Random.Range(0, Enum.GetNames(typeof(Colors)).Length);
        Vector2 randomPos = new Vector2(UnityEngine.Random.Range(-boundary.x, boundary.x), UnityEngine.Random.Range(-boundary.y, boundary.y));
        point.transform.position = randomPos;
        point.color = (Colors)randomColor;

        Addressables.LoadAssetAsync<Sprite>(spriteColorsDict[point.color].pointSprite).Completed += asyncOperationHandle => {
            //change sprite
            point.GetComponent<SpriteRenderer>().sprite = asyncOperationHandle.Result;
        };
    }

    private void OnEnable() {
        Point.OnPointAdded += InstantiateSnakeTail;
    }

    private void OnDisable() {
        Point.OnPointAdded -= InstantiateSnakeTail;
    }

    public void InstantiateSnakeTail(Point point) {
        Colors color = point.color;
        SpawnPoint();

        InternalInstantiateSnakeTail(color);
    }

    private async UniTask InternalInstantiateSnakeTail(Colors color) {
        var sprite = await Addressables.LoadAssetAsync<Sprite>(spriteColorsDict[color].tailSprite);
        var go = await Addressables.InstantiateAsync(tailReference);

        SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;

        SnakeController.Instance.AddTail(go.transform);

    }
}
