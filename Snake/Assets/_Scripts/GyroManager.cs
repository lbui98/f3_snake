using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunGames.Singleton;

public class GyroManager : Singleton<GyroManager> {
    private Gyroscope gyro;
    private Quaternion rotation;
    private bool gyroActive;


    public void EnableGyro() {
        //already enabled;
        if (gyroActive) {
            return;
        }

        if (SystemInfo.supportsGyroscope) {
            gyro = Input.gyro;
            gyro.enabled = true;
            gyroActive = gyro.enabled;
        }
        else {
            Debug.Log("Gyro is not supported on this device");
        }
    }

    private void Update() {
        if (gyroActive) {
            rotation = gyro.attitude;

        }
    }

    public Quaternion GetGyroRotation() {
        return rotation;
    }
}
