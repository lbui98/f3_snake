using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class ParentalGate : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI problem;
    [SerializeField] private TMP_InputField inputField;

    private int currentSolution;

    public void GenerateProblem() {
        //reset text field
        inputField.text = string.Empty;

        int number1 = Random.Range(1, 100);
        int number2 = Random.Range(1, 100);
        bool adding = Random.Range(0, 1f) > 0.5f;

        if (adding) {
            problem.SetText(string.Format("{0}+{1}=", number1, number2));
            currentSolution = number1 + number2;
        }
        else {
            problem.SetText(string.Format("{0}-{1}=", number1, number2));
            currentSolution = number1 - number2;
        }
    }

    public delegate void ParentalGateUnlocked(bool success);
    public static ParentalGateUnlocked OnParentalGateUnlocked;

    public void OnConfirm() {
        int result;
        bool success = int.TryParse(inputField.text.Replace(@"\s+", ""), out result);
        if (success) {
            if(result == currentSolution) {
                OnParentalGateUnlocked?.Invoke(true);
            }
        }
        else {
            inputField.transform.DOShakePosition(1f, 10f);
        }
    }

    public void OnCancelBtn() {
        OnParentalGateUnlocked?.Invoke(false);
    }
}
