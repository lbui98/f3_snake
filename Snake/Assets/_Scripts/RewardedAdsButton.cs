using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System;

public class RewardedAdsButton : MonoBehaviour  {
    [SerializeField] Button _showAdButton;
    [SerializeField] GameManager.AdRewards reward;

    private void Start() {
        if (PlayerPrefs.GetInt("AdsEnabled") == 0 || InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) Destroy(gameObject);
    }

    public void ShowRewardedVideo() {
        SoundManager.Instance.ForceMute();
        AdsManager.Instance.ShowRewardedAds();
    }

    private void OnEnable() {
        AdsManager.OnAdsDone += OnAdsDone;
    }

    private void OnDisable() {
        AdsManager.OnAdsDone -= OnAdsDone;
    }

    private void OnAdsDone() {
        SoundManager.Instance.RestoreSound();
        gameObject.SetActive(false);
        GameManager.Instance.TriggerReward(reward);
    }
}