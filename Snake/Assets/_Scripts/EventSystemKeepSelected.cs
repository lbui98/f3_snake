using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EventSystemKeepSelected : MonoBehaviour, IDeselectHandler {
    private EventSystem eventSystem;
    private GameObject lastSelected;
    private Button btn;

    public void OnDeselect(BaseEventData eventData) {
        PointerEventData ped = eventData as PointerEventData;
        if (ped == null) return;
        Debug.Log(gameObject.name + "\n" + ped.ToString());
        if (ped.pointerPressRaycast.gameObject != null) {
            if (ped.pointerPressRaycast.gameObject.GetComponent<EventSystemKeepSelected>() == null) {
                //no target or not another button, keep selected
                Debug.Log("no target");
                StartCoroutine(SelectGameObject(gameObject));
            }
            else {
                //target, another button
                StartCoroutine(SelectGameObject(ped.pointerCurrentRaycast.gameObject));
            }
        }
        else {
            Debug.Log("no target");
            StartCoroutine(SelectGameObject(gameObject));
        }
    }

    private IEnumerator SelectGameObject(GameObject target) {
        if (eventSystem.alreadySelecting) yield return new WaitForEndOfFrame();
        if (eventSystem.currentSelectedGameObject == target) yield break;
        eventSystem.SetSelectedGameObject(target);
    }

    void Start() {
        eventSystem = FindObjectOfType<EventSystem>();
        btn = GetComponent<Button>();
    }

    //void Update() {
    //    if (eventSystem != null) {
    //        if (eventSystem.currentSelectedGameObject != null) {
    //            lastSelected = eventSystem.currentSelectedGameObject;
    //        }
    //        else {
    //            eventSystem.SetSelectedGameObject(lastSelected);
    //        }
    //    }
    //}
}