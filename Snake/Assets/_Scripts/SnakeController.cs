using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunGames.Singleton;
using System;

public class SnakeController : Singleton<SnakeController> {
    [SerializeField] Rigidbody2D controllerRigidbody;

    //[SerializeField] float moveSpeed = 5f;
    [Range(0f, 50f)] [SerializeField] float acceleratorSensitivity = 0.0f;
    [SerializeField] float acceleration = 0.0f;
    [SerializeField] float maxSpeed = 0.0f;
    [SerializeField] float rotationSpeed = 0.0f;
    [SerializeField] Vector2 tailOffset;
    [SerializeField] Transform snakeTailContainer;
    [SerializeField] int snakeCollisionStart = 4;
    [SerializeField] float snakeTailLag = 0.1f;

    private List<Transform> snakeTails = new List<Transform>();
    private Vector2 movementInput;

    private bool canMove = true;
    private bool invincible = false;

    private void OnEnable() {
        GameManager.OnGameOver += OnGameOver;
        GameManager.OnReceiveReward += OnReceiveReward;
    }

    private void OnDisable() {
        GameManager.OnGameOver -= OnGameOver;
        GameManager.OnReceiveReward -= OnReceiveReward;
    }

    private void Update() {
        if (!canMove) return;

        Vector3 dir = Input.acceleration;
        movementInput = new Vector2(dir.x, dir.y) * acceleratorSensitivity;
    }

    private void FixedUpdate() {
        if (!canMove) return;

        //MoveVelocity();
        MoveForce();
        Rotate();
    }

    private void MoveForce() {
        controllerRigidbody.velocity = Vector2.zero;
        var force = movementInput * (acceleration * 50.0f);// + (speed * 8.0 * (SnakeEnums.elementsList.Count)));
        controllerRigidbody.AddForce(force);

        //Vector2 velocity = controllerRigidbody.velocity;
        //// Clamp horizontal speed.
        //velocity.x = Mathf.Clamp(velocity.x, -maxSpeed, maxSpeed);
        //velocity.y = Mathf.Clamp(velocity.y, -maxSpeed, maxSpeed);

        //// Assign back to the body.
        //controllerRigidbody.velocity = velocity;
    }

    private void MoveVelocity() {
        Vector2 velocity = controllerRigidbody.velocity;

        // Apply acceleration directly as we'll want to clamp
        // prior to assigning back to the body.
        velocity += movementInput * acceleration * Time.fixedDeltaTime;

        // We've consumed the movement, reset it.
        movementInput = Vector2.zero;

        // Clamp horizontal speed.
        velocity.x = Mathf.Clamp(velocity.x, -maxSpeed, maxSpeed);
        velocity.y = Mathf.Clamp(velocity.y, -maxSpeed, maxSpeed);

        // Assign back to the body.
        controllerRigidbody.velocity = velocity;
    }

    private void Rotate() {
        Vector2 moveDirection = movementInput;
        if (moveDirection != Vector2.zero) {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            var qRot = Quaternion.Euler(new Vector3(0, 0, angle));
            transform.rotation = Quaternion.Slerp(transform.rotation, qRot, rotationSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (snakeTails.Count < snakeCollisionStart || !canMove || invincible) return;
        if (collider.CompareTag(bunGames.Tools.Tags.SNAKE_TAIL)) {
            //collided with tail
            //game over
            SoundManager.PlaySFX(SoundManager.Sounds.TAIL);
            Handheld.Vibrate();
            GameManager.Instance.TriggerGameOver(GameManager.GameOverReason.COLLISION);
            canMove = false;
            controllerRigidbody.velocity = Vector2.zero;
            controllerRigidbody.freezeRotation = true;
        }
    }

    public void AddTail(Transform tail) {
        SoundManager.PlaySFX(SoundManager.Sounds.POINT);
        tail.SetParent(snakeTailContainer);

        Transform lastTail;
        if (snakeTails.Count == 0) {
            lastTail = transform;
        }
        else {
            lastTail = snakeTails[snakeTails.Count - 1];
        }

        if (snakeTails.Count < snakeCollisionStart) {
            Destroy(tail.gameObject.GetComponent<Collider2D>());
        }

        tail.rotation = lastTail.rotation;
        tail.position = lastTail.position + new Vector3(tailOffset.x, 0, tailOffset.y);
        SnakeTail snakeTail = tail.gameObject.AddComponent<SnakeTail>();
        snakeTail.leader = lastTail;
        snakeTail.lagSeconds = snakeTailLag;
        snakeTails.Add(tail);
    }

    private void OnGameOver(GameManager.GameOverReason reason) {
        canMove = false;
    }


    private void OnReceiveReward(GameManager.AdRewards reward) {
        canMove = true;
        controllerRigidbody.freezeRotation = false;
        StartCoroutine(SetInvincibility(1.5f));
    }

    private IEnumerator SetInvincibility(float seconds) {
        invincible = true;
        yield return new WaitForSeconds(seconds);
        invincible = false;
    }
}
