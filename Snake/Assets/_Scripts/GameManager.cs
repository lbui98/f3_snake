using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunGames.Singleton;
using UnityEngine.Advertisements;

public class GameManager : PersistentSingleton<GameManager> {
    private bool paused = false;

    public int points = 0;
    public int timeLimit = 60;

    public enum GameOverReason { COLLISION, TIMER }
    public enum PlayStyles { TIMED, ARCADE }
    public enum AdRewards { NEW_LIFE, MORE_TIME }

    public int TimeLimit {
        get {
            if (currentPlayStyle == PlayStyles.TIMED) return timeLimit;
            return 0;
        }
    }

    public PlayStyles currentPlayStyle;

    public delegate void GameOver(GameOverReason reason);
    public static event GameOver OnGameOver;

    public void TriggerGameOver(GameOverReason reason) {
        OnGameOver?.Invoke(reason);
    }

    public delegate void ReceiveReward(AdRewards reward);
    public static event ReceiveReward OnReceiveReward;

    public void TriggerReward(AdRewards reward) {
        OnReceiveReward?.Invoke(reward);
    }

    public bool PauseGame() {
        paused = !paused;
        Time.timeScale = paused ? 0f : 1f;
        return paused;
    }

    public void Start() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void OnApplicationQuit() {
        Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }
}