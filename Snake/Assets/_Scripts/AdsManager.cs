using UnityEngine;
using UnityEngine.Advertisements;
using bunGames.Singleton;

public class AdsManager : PersistentSingleton<AdsManager>, IUnityAdsInitializationListener, IUnityAdsListener, IUnityAdsLoadListener {
#if UNITY_IOS
    private const string _gameId =  "4435089";
    private const string Rewarded_Id = "Rewarded_iOS";
#elif UNITY_ANDROID
    private const string _gameId = "4435088";
    private const string Rewarded_Id = "Rewarded_Android";
#endif

    [SerializeField] bool _testMode = true;
    [SerializeField] bool _enablePerPlacementMode = true;

    public delegate void AdsDone();
    public static event AdsDone OnAdsDone;

    void Awake() {
        if (PlayerPrefs.HasKey("AdsEnabled")) InitializeAds();
    }

    public void InitializeAds() {
        Advertisement.AddListener(this);
        Advertisement.Initialize(_gameId, _testMode, _enablePerPlacementMode, this);
    }

    public void OnInitializationComplete() {
        Debug.Log("Unity Ads initialization complete.");
        Advertisement.Load(Rewarded_Id, this);
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message) {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }

    public void OnUnityAdsDidError(string message) {
        Debug.Log("Ad encountered an error: " + message);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
        if (showResult == ShowResult.Finished) {
            Debug.Log("Unity Ads finished.");
            OnAdsDone?.Invoke();
            Advertisement.Load(Rewarded_Id, this);
        }
    }

    public void OnUnityAdsDidStart(string placementId) {
        Debug.Log("Ads did start");
    }

    public void OnUnityAdsReady(string placementId) {
        Debug.Log("Ads ready");

    }

    public void OnUnityAdsAdLoaded(string placementId) {
        Debug.Log("Unity Ads loaded");
    }

    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message) {
        Debug.Log("Ad encountered an error: " + message);
    }

    public void ShowRewardedAds() {
        if (Advertisement.IsReady(Rewarded_Id)) {
            Advertisement.Show(Rewarded_Id);
        }
    }
}
