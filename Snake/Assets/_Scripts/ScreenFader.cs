using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenFader : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        Image img = GetComponentInChildren<Image>();
        img.DOFade(0f, 1f);
        Destroy(gameObject, 1.1f);
    }
}
