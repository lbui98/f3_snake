using Cysharp.Threading.Tasks;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
#if UNITY_IOS
using Unity.Advertisement.IosSupport;
#endif

public class UIMenu : MonoBehaviour {
    [SerializeField] private Image muteBtn;
    [SerializeField] private Sprite[] muteImages;
    [SerializeField] private GameObject panelSettings;
    [SerializeField] private AssetReference ArcadeScene, TimedScene;
    [SerializeField] private int LocalAracadeSceneId, LocalTimedSceneId;
    [SerializeField]
    private TextMeshProUGUI
        playStyleDescription,
        arcadeHighscoreText,
        timedHighscoreText;
    private AssetReference currentScene;
    private int currentSceneId;

    [Header("Settings")]
    [SerializeField] private Toggle adsToggle;
    [SerializeField]
    private GameObject
        adSettingsPanel,
        aboutUsPanel;
    private ParentalGate parentalGate;

    [Header("Download")]
    [SerializeField] private GameObject DownloadPanel;
    [SerializeField] private GameObject MainPanel;
    [SerializeField] private GameObject WarningPanel;
    [SerializeField] private Button[] buttons;
    private static bool initialized = false;

    public ATTrackingStatusBinding.AuthorizationTrackingStatus status;

    private IEnumerator Start() {
        WarningPanel.SetActive(false);
        if (!PlayerPrefs.HasKey("INITIAL_LOAD") && InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            //Player downloaded the game but did not download the addressables yet
            //display Warning
            WarningPanel.SetActive(true);
            yield break;
        }
        Debug.Log("BUNNY Proceeding with download");
        if (!initialized && InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified) {
            yield return Addressables.InitializeAsync();
            var op = Addressables.DownloadDependenciesAsync("Preload");
            Slider downloadProgress = DownloadPanel.GetComponentInChildren<Slider>(true);
            while (!op.IsDone) {
                downloadProgress.value = op.GetDownloadStatus().Percent;
                yield return null;
            }
            downloadProgress.value = 1f;
            yield return new WaitForEndOfFrame();
            SoundManager.Instance.Initialize();
            initialized = true;
            PlayerPrefs.SetInt("INITIAL_LOAD", 1);
            PlayerPrefs.Save();
        }
        Destroy(DownloadPanel);
        MainPanel.SetActive(true);

        yield return LoadLocalization();
        parentalGate = GetComponentInChildren<ParentalGate>(true);
        CheckAdsSettings();
        OnAboutUsBtn();

        OnPlayStyleBtn((int)GameManager.PlayStyles.TIMED);
        int imgId = !SoundManager.Instance.muted ? 0 : 1;
        muteBtn.sprite = muteImages[imgId];
    }

    public void OnRetry() {
        StartCoroutine(Start());
    }

    private async UniTask LoadLocalization() {
        string highscoreLabel = await bunGames.Localization.LocalizationManager.GetLocalizedUIString("UI_HIGHSCORE");
        if (PlayerPrefs.HasKey("ArcadeHighscore")) {
            arcadeHighscoreText.SetText(string.Format(highscoreLabel, PlayerPrefs.GetInt("ArcadeHighscore")));
            arcadeHighscoreText.gameObject.SetActive(true);
        }

        if (PlayerPrefs.HasKey("TimedHighscore")) {
            timedHighscoreText.SetText(string.Format(highscoreLabel, PlayerPrefs.GetInt("TimedHighscore")));
            timedHighscoreText.gameObject.SetActive(true);
        }
    }

    private void OnEnable() {
        ParentalGate.OnParentalGateUnlocked += OnParentalGateUnlocked;
    }

    private void OnDisable() {
        ParentalGate.OnParentalGateUnlocked -= OnParentalGateUnlocked;
    }

    private void OnParentalGateUnlocked(bool success) {
        panelSettings.SetActive(success);
        parentalGate.gameObject.SetActive(false);
    }

    private void CheckAdsSettings() {
        if (PlayerPrefs.HasKey("AdsEnabled")) return;

        OnSettingsBtn();
    }

    public void OnPlayBtn() {
        SoundManager.PlayBGM(SoundManager.Sounds.GAME_BGM);
        SoundManager.PlaySFX(SoundManager.Sounds.POINT);

        currentScene = GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.ARCADE ? ArcadeScene : TimedScene;
        Addressables.LoadResourceLocationsAsync(currentScene.RuntimeKey).Completed += handle => {
            if (handle.Status != UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded) {
                //load scene from server
                //StartCoroutine(InternalLoadScene());
                Debug.Log("BUNNY LOADING SCENE REMOTE");
                var op = Addressables.LoadSceneAsync(currentScene);
            }
            else {
                if (handle.Result.Count > 0) {
                    //load scene from server
                    //StartCoroutine(InternalLoadScene());
                    Debug.Log("BUNNY LOADING SCENE REMOTE");
                    var op = Addressables.LoadSceneAsync(currentScene);
                }
                else {
                    Debug.Log("BUNNY LOADING SCENE LOCAL");
                    currentSceneId = GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.ARCADE ? LocalAracadeSceneId : LocalTimedSceneId;
                    SceneManager.LoadSceneAsync(currentSceneId);
                }
            }
        };
    }

    //private IEnumerator InternalLoadScene() {
    //    currentScene = GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.ARCADE ? AracadeScene : TimedScene;
    //    var op = Addressables.LoadSceneAsync(currentScene);
    //}

    public void OnPlayStyleBtn(int playStyle) {
        for (int i = 0; i < buttons.Length; i++) {
            buttons[i].animator.SetTrigger(buttons[i].animationTriggers.normalTrigger);
        }

        buttons[playStyle].animator.SetTrigger(buttons[playStyle].animationTriggers.selectedTrigger);

        GameManager.Instance.currentPlayStyle = (GameManager.PlayStyles)playStyle;
        SetDescription((GameManager.PlayStyles)playStyle);
    }

    private async UniTask SetDescription(GameManager.PlayStyles playStyle) {
        string desc = await bunGames.Localization.LocalizationManager.GetLocalizedUIString("DESCRIPTION_PLAYSTYLE_" + playStyle.ToString());
        if (playStyle == GameManager.PlayStyles.TIMED) {
            desc = string.Format(desc, GameManager.Instance.timeLimit);
        }
        playStyleDescription.SetText(desc);
    }

    public void OnMute() {
        bool muted = SoundManager.Instance.OnMute();
        int imgId = !muted ? 0 : 1;
        muteBtn.sprite = muteImages[imgId];
    }

    public void OnSettingsBtn() {
        if (panelSettings.activeSelf == false) {
            //activate parental gate
            adsToggle.isOn = PlayerPrefs.GetInt("AdsEnabled") == 1;
            parentalGate.gameObject.SetActive(true);
            parentalGate.GenerateProblem();

#if UNITY_IOS
            //Version currentVersion = new Version(UnityEngine.iOS.Device.systemVersion);
            //Version ios14 = new Version("14.5");
            //if (currentVersion < ios14) return;

            //var status = ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
            if (status == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED) {
                //request apple tracking permission
                ATTrackingStatusBinding.RequestAuthorizationTrackingCompleteHandler requestAuthorizationTrackingComplete = OnAuthoritationTracking;
                ATTrackingStatusBinding.RequestAuthorizationTracking(requestAuthorizationTrackingComplete);
            }
            else if (status == ATTrackingStatusBinding.AuthorizationTrackingStatus.DENIED) {
                adsToggle.interactable = false;
            }
            else {
                adsToggle.interactable = true;
            }
#endif
        }
        else {
            PlayerPrefs.SetInt("AdsEnabled", adsToggle.isOn ? 1 : 0);
            PlayerPrefs.Save();
            if (adsToggle.isOn) AdsManager.Instance.InitializeAds();
            panelSettings.SetActive(false);
        }
    }

    public void OnAdsSettingsBtn() {
        adSettingsPanel.SetActive(true);
        aboutUsPanel.SetActive(false);
    }

    public void OnAboutUsBtn() {
        adSettingsPanel.SetActive(false);
        aboutUsPanel.SetActive(true);
    }

    public void OnToggleValueChanged(Toggle change) {
#if UNITY_IOS
        //if (ATTrackingStatusBinding.GetAuthorizationTrackingStatus() == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED) {
        //    //request apple tracking permission
        //    ATTrackingStatusBinding.RequestAuthorizationTrackingCompleteHandler requestAuthorizationTrackingComplete = OnAuthoritationTracking;
        //    ATTrackingStatusBinding.RequestAuthorizationTracking(requestAuthorizationTrackingComplete);
        //}
#endif
    }

#if UNITY_IOS
    private void OnAuthoritationTracking(int status) {
        Debug.Log("BUNNY: Authorization status is " + status);
        if ((ATTrackingStatusBinding.AuthorizationTrackingStatus)status != ATTrackingStatusBinding.AuthorizationTrackingStatus.AUTHORIZED) {
            adsToggle.interactable = false;
        }
        else {
            adsToggle.interactable = true;
        }
    }
#endif
}