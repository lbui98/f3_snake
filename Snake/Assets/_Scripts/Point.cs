using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Point : MonoBehaviour {
    public SnakeColorManager.Colors color;

    public delegate void PointAdded(Point point);
    public static event PointAdded OnPointAdded;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag(bunGames.Tools.Tags.PLAYER)) {
            GameManager.Instance.points++;
            OnPointAdded?.Invoke(this);
        }
    }
}
