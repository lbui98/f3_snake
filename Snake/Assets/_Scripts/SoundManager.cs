using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunGames.Singleton;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

public class SoundManager : PersistentSingleton<SoundManager> {
    public enum Sounds { POINT, TAIL, CLOCK, FINAL, MENU_BGM, GAME_BGM };

    [System.Serializable]
    public class Sound {
        public Sounds sound;
        public AssetReference audioClip;
        public AssetReference localAudioClip;
    }

    public AudioSource BGMSource;
    public bool muted = false;

    [SerializeField] private Sound[] soundClips;

    private Dictionary<Sounds, AudioClip> audioDict = new Dictionary<Sounds, AudioClip>();


    private void Start() {
        BGMSource.volume = muted ? 0f : 1f;
    }

    public bool OnMute() {
        muted = !muted;
        BGMSource.volume = muted ? 0f : 1f;
        return muted;
    }

    public void ForceMute() {
        BGMSource.volume = 0f;
    }

    public void RestoreSound() {
        BGMSource.volume = muted ? 0f : 1f;
    }

    public static void PlaySFX(Sounds sound) {
        if (Instance.muted) return;
        if (Instance.audioDict.ContainsKey(sound)) {
            Instance.PlaySFX(Instance.audioDict[sound]);
        }
    }

    public static void PlayBGM(Sounds sound) {
        if (Instance.audioDict.ContainsKey(sound)) {
            Instance.BGMSource.clip = Instance.audioDict[sound];
            Instance.BGMSource.Play();
        }
    }

    private void PlaySFX(AudioClip clip) {
        GameObject go = new GameObject("Audio Source");
        AudioSource audio = go.AddComponent<AudioSource>();
        audio.loop = false;
        audio.volume = 1f;
        audio.clip = clip;
        audio.Play();
        Destroy(go, clip.length);
    }

    public void Initialize() {
        LoadClips();
    }

    private async UniTask LoadClips() {
        for (int i = 0; i < soundClips.Length; i++) {
            AudioClip audioClip;
            var res = Addressables.LoadResourceLocationsAsync(soundClips[i].audioClip.RuntimeKey);
            await res;
            if (res.Status == UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded) {
                if (res.Result.Count > 0) {
                    audioClip = await Addressables.LoadAssetAsync<AudioClip>(soundClips[i].audioClip);
                }
                else {
                    audioClip = await Addressables.LoadAssetAsync<AudioClip>(soundClips[i].localAudioClip);
                }
            }
            else {
                audioClip = await Addressables.LoadAssetAsync<AudioClip>(soundClips[i].localAudioClip);
            }
            audioDict.Add(soundClips[i].sound, audioClip);
        }

        soundClips = null;
    }
}
