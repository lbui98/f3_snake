using UnityEngine;
using bunGames.Singleton;
using UnityEngine.Localization;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

namespace bunGames.Localization {
    public class LocalizationManager : PersistentSingleton<LocalizationManager> {
        [SerializeField] private LocalizedStringTable m_ui;

        public LocalizedStringTable UI { get => m_ui; }

        public async static UniTask<string> GetLocalizedString(LocalizedStringTable table, string key) => await InternalGetLocalizedString(table, key);

        public async static UniTask<string> GetLocalizedString(LocalizedString localizedString) => await InternalGetLocalizedString(localizedString);

        public async static UniTask<string> GetLocalizedUIString(string key) => await GetLocalizedString(Instance.UI, key);

        private async static UniTask<string> InternalGetLocalizedString(LocalizedStringTable table, string key) {
            var asyncOperationHandle = await table.GetTableAsync();

            var entry = asyncOperationHandle.GetEntry(key);
            if (entry == null) {
                Debug.LogWarning("LOCALIZATION: " + key + " could not be found");
                //Addressables.Release(asyncOperationHandle);
                return null;
            }

            string result = entry.LocalizedValue;
            //Addressables.Release(asyncOperationHandle);

            return result;
        }

        private async static UniTask<string> InternalGetLocalizedString(LocalizedString localiizedString) {
            var asyncOperationHandle = await localiizedString.GetLocalizedStringAsync();

            var result = asyncOperationHandle;
            //Addressables.Release(asyncOperationHandle);

            return result;
        }
    }
}