using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunGames.Singleton;
using System;

public class TimeManager : Singleton<TimeManager> {
    private int timeLeft;
    private float timer;

    private bool ongoing = true;

    public delegate void TimePassed(int timeLeft);
    public static event TimePassed OnTimePassed;

    private void Start() {
        timeLeft = GameManager.Instance.TimeLimit;
    }

    private void Update() {
        if (timeLeft <= 0 || !ongoing) return;

        if (timer < 1f) {
            timer += Time.deltaTime;
        }
        else {
            timeLeft--;
            OnTimePassed(timeLeft);
            timer = 0;
        }
    }

    private void OnEnable() {
        GameManager.OnGameOver += OnGameOver;
        GameManager.OnReceiveReward += OnReceiveReward;
    }

    private void OnDisable() {
        GameManager.OnGameOver -= OnGameOver;
        GameManager.OnReceiveReward -= OnReceiveReward;
    }

    private void OnReceiveReward(GameManager.AdRewards reward) {
        if (reward == GameManager.AdRewards.MORE_TIME) {
            timeLeft += GameManager.Instance.TimeLimit;
        }
        else if (reward == GameManager.AdRewards.NEW_LIFE && GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.TIMED) {
            timeLeft += Mathf.RoundToInt(GameManager.Instance.TimeLimit * 1f/3f);
        }
        if (GameManager.Instance.currentPlayStyle == GameManager.PlayStyles.TIMED) {
            ongoing = true;
            OnTimePassed(timeLeft);
        }
    }

    private void OnGameOver(GameManager.GameOverReason reason) {
        ongoing = false;
    }
}
