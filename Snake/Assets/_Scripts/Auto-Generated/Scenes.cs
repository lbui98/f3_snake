//This class is auto-generated do not modify
namespace bunGames.Tools
{
	public static class Scenes
	{
		public const string SPLASH_SCREEN = "SplashScreen";
		public const string MENU_SCENE = "MenuScene";
		public const string GAME_SCENE = "GameScene";
		public const string END_GAME_TIMER_SCENE = "EndGameTimerScene";
		public const string END_GAME_COLLISION_SCENE = "EndGameCollisionScene";

		public const int TOTAL_SCENES = 5;


		public static int nextSceneIndex()
		{
			if( UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1 == TOTAL_SCENES )
				return 0;
			return UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1;
		}
	}
}