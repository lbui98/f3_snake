// This class is auto-generated do not modify
namespace bunGames.Tools
{
	public static class Layers
	{
		public const int DEFAULT = 0;
		public const int TRANSPARENT_FX = 1;
		public const int IGNORE_RAYCAST = 2;
		public const int WATER = 4;
		public const int UI = 5;
		public const int BACKGROUND = 8;
		public const int CAMERA = 9;
		public const int GUI_ELEMENTS = 10;
		public const int POINT = 11;
		public const int WALLS = 12;
		public const int SNAKE_HEAD = 13;
		public const int SNAKE_TAIL = 14;


		public static int onlyIncluding( params int[] layers )
		{
			int mask = 0;
			for( var i = 0; i < layers.Length; i++ )
				mask |= ( 1 << layers[i] );
			return mask;
		}
		public static int everythingBut( params int[] layers )
		{
			return ~onlyIncluding( layers );
		}
	}
}