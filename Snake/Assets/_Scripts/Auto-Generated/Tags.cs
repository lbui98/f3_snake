//This class is auto-generated do not modify
namespace bunGames.Tools
{
	public static class Tags
	{
		public const string UNTAGGED = "Untagged";
		public const string RESPAWN = "Respawn";
		public const string FINISH = "Finish";
		public const string EDITOR_ONLY = "EditorOnly";
		public const string MAIN_CAMERA = "MainCamera";
		public const string PLAYER = "Player";
		public const string GAME_CONTROLLER = "GameController";
		public const string BACKGROUND = "Background";
		public const string SNAKE_TAIL = "Snake Tail";
		public const string SNAKE_HEAD = "Snake Head";
		public const string SNAKE_BODY = "Snake Body";
		public const string GUI = "GUI";
		public const string POINT = "Point";
		public const string WALLS = "Walls";
	}
}