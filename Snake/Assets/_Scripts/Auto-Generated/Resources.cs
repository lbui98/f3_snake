//This class is auto-generated do not modify
namespace bunGames.Tools
{
	public static class Resources
	{
		public const string ALL_IN1ONE_SHADER_FUNCTIONS = "AllIn1OneShaderFunctions";
		public const string ALL_IN1SPRITE_SHADER = "AllIn1SpriteShader";
		public const string ALL_IN1SPRITE_SHADER_SCALED_TIME = "AllIn1SpriteShaderScaledTime";
		public const string ALL_IN1SPRITE_SHADER_UI_MASK = "AllIn1SpriteShaderUiMask";
		public const string LETTERBOX = "Letterbox";
		public const string PC2D_BLINDS = "PC2D_Blinds";
		public const string PC2D_CIRCLE = "PC2D_Circle";
		public const string PC2D_FADE = "PC2D_Fade";
		public const string PC2D_SHUTTERS = "PC2D_Shutters";
		public const string PC2D_TEXTURE = "PC2D_Texture";
		public const string PC2D_WIPE = "PC2D_Wipe";
	}
}